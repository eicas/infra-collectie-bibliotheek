#!/bin/bash

export MYSQL_CONTAINER_ID=$(docker ps | grep mysql | cut -d " " -f 1)
export PASSWORD=$(cat .env.bieb | cut -d ":" -f 3 | cut -d "@" -f 1)
echo $PASSWORD | docker exec -i $MYSQL_CONTAINER_ID mysqldump bieb -u bieb -p > dumps/bieb-$(date +%Y%m%d).sql
export PASSWORD=$(cat .env.collectie | cut -d ":" -f 3 | cut -d "@" -f 1)
echo $PASSWORD | docker exec -i $MYSQL_CONTAINER_ID mysqldump omeka -u omeka -p > dumps/collectie-$(date +%Y%m%d).sql
